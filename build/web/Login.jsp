<%-- 
    Document   : Login
    Created on : Oct 19, 2023, 1:00:19 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>LACOSTE</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>

    <body>
        <!-- Page Preloder -->
        <div id="preloder">
            <div class="loader"></div>
        </div>
        <div class="contact-form spad">
            <div class="container">
                <div class="row">
                </div>
                <form action="#">
                    <div class="humberger__menu__logo">
                        <a href="#"><img src="img/logo2.png" alt=""></a><br>
                    </div>
                    <div class="row" style="margin-top: 50px">
                        <div class="col-lg-4 text-center">
                            <a href="CreateAccount.jsp" class="site-btn">CREATE ACCOUNT</a>
                        </div>
                        <div class="col-lg-4 text-center">
                            <a href="LoginAccount.jsp" class="site-btn">LOGIN ACCOUNT</a>
                        </div>
                        <div class="col-lg-4 text-center">
                            <a href="HomeController" class="site-btn">BACK TO HOMEPAGE</a>
                        </div>
                    </div>
            </div>
        </form>
    </div>
</div>

<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/mixitup.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>
</body>
</html>
