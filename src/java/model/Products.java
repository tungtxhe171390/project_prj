/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.List;

/**
 *
 * @author Acer
 */
public class Products {

    private int iditem;
    private String item;
    private String material;
    private float price;
    private int quantity;
    private String catid;
    private String image;

    public Products() {
    }

    public Products(int iditem, String item, String material, float price, int quantity, String catid, String image) {
        this.iditem = iditem;
        this.item = item;
        this.material = material;
        this.price = price;
        this.quantity = quantity;
        this.catid = catid;
        this.image = image;
    }

    public int getIditem() {
        return iditem;
    }

    public void setIditem(int iditem) {
        this.iditem = iditem;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return String.format("%-15d %-10s %-40s %-10.2f %-10d %-15s %-15s",
                iditem, item, material, price, quantity, catid, image);
    }

    public void display(List<Products> list) {
        System.out.println(String.format("%-15s %-10s %-40s %-10s %-10s %-15s %-15s",
                "iditem", "item", "material", "price", "quantity", "catid", "image"));

        for (Products o : list) {
            System.out.println(o.toString());
        }
    }
}
