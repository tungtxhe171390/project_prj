/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.List;

/**
 *
 * @author Acer
 */
public class Cart {

    List<Item> items;

    public Cart() {
    }

    public Cart(List<Item> items) {
        this.items = items;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    private Item getItemById(int id) {//tra ve null neu ko co trg cart
        for (Item i : items) {
            if (i.getProduct().getIditem() == id) {
                return i;
            }
        }
        return null;
    }

    public int getQuantityByID(int id) {
        return getItemById(id).getQuantity();//cong don product
    }

    public void addItem(Item t) {
        if (getItemById(t.getProduct().getIditem()) != null) {
            Item m = getItemById(t.getProduct().getIditem());//get obj ra , sau do get quantity
            m.setQuantity(m.getQuantity() + t.getQuantity());//quantity ban dau + quantity sau 
        }
        else {
            items.add(t);
        }
    }

    public void removeItem(int id) {
        if (getItemById(id) != null) {
            items.remove(getItemById(id));
        }
    }

    public double getTotalMoney() {
        double t = 0;
        for (Item i : items) {
            t += (i.getQuantity() * i.getProduct().getIditem());
        }
        return t;
    }
}
