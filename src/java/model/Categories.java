/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author Acer
 */
public class Categories {

    private String catid;
    private String code;
    private String cname;

    public Categories() {
    }

    public Categories(String catid, String code, String cname) {
        this.catid = catid;
        this.code = code;
        this.cname = cname;
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCname() {
        return cname;
    }

    public void setCname(String cname) {
        this.cname = cname;
    }

    @Override
    public String toString() {
        return "Categories{" + "catid=" + catid + ", code=" + code + ", cname=" + cname + '}';
    }

}
