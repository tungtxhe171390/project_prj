/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.Date;

/**
 *
 * @author Acer
 */
public class Bill {

    int id;
    Date date;
    String catid;
    float totalmoney;

    public Bill() {
    }

    public Bill(int id, Date date, String catid, float totalmoney) {
        this.id = id;
        this.date = date;
        this.catid = catid;
        this.totalmoney = totalmoney;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public float getTotalmoney() {
        return totalmoney;
    }

    public void setTotalmoney(float totalmoney) {
        this.totalmoney = totalmoney;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", date=" + date + ", catid=" + catid + ", totalmoney=" + totalmoney + '}';
    }

}
