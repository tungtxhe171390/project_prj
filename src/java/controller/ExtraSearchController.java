/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import dal.daoCate;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;
import model.Categories;
import model.Material;
import model.Products;

/**
 *
 * @author Acer
 */
public class ExtraSearchController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ExtraSearchController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ExtraSearchController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String sql = "";

        String[] selectedCatIds = request.getParameterValues("catid");

        String s1 = "";
        for (int i = 0; i < selectedCatIds.length; i++) {
            s1 += " catid = '" + selectedCatIds[i] + "' or ";
        }
        s1 = s1.substring(0, s1.length() - 3);

        String[] selectedMaterials = request.getParameterValues("material");

        String s2 = "";
        for (int i = 0; i < selectedMaterials.length; i++) {
            s2 += " material = '" + selectedMaterials[i] + "' or ";
        }
        s2 = s2.substring(0, s2.length() - 3);

        sql += "Select * From Products Where (" + s1 + ") and (" + s2 + ") ";

        DAO dao = new DAO();
        daoCate d = new daoCate();
        List<Products> list = dao.getProductsByCatIdAndMaterial(sql);
        request.setAttribute("listP", list);

//        out.println("Selected Cat Ids: " + Arrays.toString(selectedCatIds));
//        out.println("Selected Materials: " + Arrays.toString(selectedMaterials));
//        out.println("Generated SQL: " + sql);
//        out.println("Number of Products: " + list.toString());
        List<Products> price = dao.getPriceAsc();
        List<Products> quantity = dao.getQuantityAsc();
        List<Products> a = dao.getProductAlphabet();
        request.setAttribute("a", a);
        request.setAttribute("price", price);
        request.setAttribute("quantity", quantity);

        List<Categories> listC = d.getAllCategories();
        List<Material> listM = dao.getAllMaterial();
        request.setAttribute("listM", listM);
        request.setAttribute("listCC", listC);

        request.getRequestDispatcher("Home.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
