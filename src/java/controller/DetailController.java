/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.DAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Cart;
import model.Item;
import model.Products;

/**
 *
 * @author Acer
 */
public class DetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String iditem = request.getParameter("iditem");

        DAO dao = new DAO();
        Products p = dao.getProductsByIditem(iditem);

        request.setAttribute("detail", p);
        request.getRequestDispatcher("Detail.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
        throws ServletException, IOException {
    HttpSession session = request.getSession(true);
    Cart cart = null;
    Object o = session.getAttribute("cart");

    // Kiểm tra xem giỏ hàng đã tồn tại trong phiên chưa
    if (o != null) {
        cart = (Cart) o;
    } else {
        cart = new Cart();
    }

    String tnum = request.getParameter("num");
    String iditem = request.getParameter("iditem");
    int num;

    try {
        num = Integer.parseInt(tnum);
        DAO dao = new DAO();
        Products p = dao.getProductsByIditem(iditem);

        float price = p.getPrice() * 2;
        Item i = new Item(p, num, price);
        cart.addItem(i);
    } catch (NumberFormatException e) {
        num = 1;
    }

    List<Item> list = cart.getItems();
    session.setAttribute("cart", cart); // Cập nhật giỏ hàng trong phiên của người dùng
    session.setAttribute("size", list.size()); // Cập nhật số lượng mặt hàng trong biến "size"
    request.getRequestDispatcher("Detail.jsp").forward(request, response);
}


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
