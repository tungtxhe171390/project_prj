/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import model.Account;
import model.Cart;
import model.Item;

/**
 *
 * @author Acer
 */
public class OrderDAO extends DBContext{
    public void addOrder(Account a, Cart cart) {
        LocalDate curDate = java.time.LocalDate.now();
        String date = curDate.toString();
        try{
            //add vào bảng Bill
            String sql="Insert int [Bill] values(?, ?, ?)";
             PreparedStatement st = connection.prepareStatement(sql);
             st.setString(1, date);
             st.setString(2, a.getUsername());
             st.setDouble(3, cart.getTotalMoney());
             st.executeUpdate();
             //lay ra id cua Bill vua add
             String sql1="Select top 1 id from [Bill] order by id desc";
             PreparedStatement st1= connection.prepareStatement(sql1);
             ResultSet rs=st1.executeQuery();
             //add vào bảng OrderLine
             if(rs.next()){
                 int oid = rs.getInt(1);
                 for(Item i: cart.getItems()){
                     String sql2="insert into [OrderLine] values(?, ?, ?, ?)";
                     PreparedStatement st2 = connection.prepareStatement(sql2);
                     st2.setInt(1, oid);
                      st2.setInt(2, i.getProduct().getIditem());
                      st2.setInt(3, i.getQuantity());
                      st2.setDouble(4, i.getPrice());
                      st2.executeUpdate();
                 }
             }
        }catch(SQLException e){
            System.out.println(e);
        }
    }
}
