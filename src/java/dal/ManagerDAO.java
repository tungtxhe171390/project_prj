/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Products;

/**
 *
 * @author Acer
 */
public class ManagerDAO extends DBContext {

    public boolean updateProductByIditem(int iditem, String item, String material, float price, int quantity, String image) {
        String sql = "UPDATE Products SET item=?, material=?, price=?, quantity=?, image=? WHERE iditem=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, item);
            st.setString(2, material);
            st.setFloat(3, price);
            st.setInt(4, quantity);
            st.setString(5, image);
            st.setInt(6, iditem);

            int updatedRows = st.executeUpdate();
            if (updatedRows > 0) {
                return true;
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public Products removeByIditem(String iditem) {
        String sql = "DeLete Products\n"
                + "where iditem=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, iditem);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Products(
                        rs.getInt("iditem"),
                        rs.getString("item"),
                        rs.getString("material"),
                        rs.getFloat("price"),
                        rs.getInt("quantity"),
                        rs.getString("catid"),
                        rs.getString("image")
                );
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean insertProduct(int iditem, String item, String material, float price, int quantity, String catid, String image) {
        String sql = "INSERT INTO Products (iditem, item, material, price, quantity, catid, image) "
                + "VALUES (?, ?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, iditem);
            st.setString(2, item);
            st.setString(3, material);
            st.setFloat(4, price);
            st.setInt(5, quantity);
            st.setString(6, catid);
            st.setString(7, image);

            int rowsInserted = st.executeUpdate();
            return rowsInserted > 0; // Kiểm tra xem có chèn thành công hay không
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

    public boolean changePassword(String password, String username) {
        String sql = "UPDATE Accounts\n"
                + "SET password = ?\n"
                + "WHERE username = ? AND isCustomer = 1 AND isAdmin = 0";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, password);
            st.setString(2, username);

            int updatedRows = st.executeUpdate();
            return updatedRows > 0;
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

}
