/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;

/**
 *
 * @author Acer
 */
public class AccountDAO extends DBContext {

    public List<Account> getAllAccounts() {
        List<Account> list = new ArrayList<>();
        String sql = "SELECT * FROM Accounts";
        System.out.println("SQL Query: " + sql);

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account a = new Account(
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getInt("isCustomer"),
                        rs.getInt("isAdmin")
                );
                list.add(a);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Account removeByUser(String user) {
        String sql = "DeLete Accounts\n"
                + "where username=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(
                        rs.getString("username"),
                        rs.getString("password"),
                        rs.getInt("isCustomer"),
                        rs.getInt("isAdmin")
                );
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean updateAccountByUser(String username) {
        String sql = "UPDATE Accounts SET isCustomer=0, isAdmin=1 WHERE username=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);

            int updatedRows = st.executeUpdate();
            if (updatedRows > 0) {
                return true;
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return false;
    }

//    public Account getAccountsByUser(String user) {
//        String sql = "Select * from Accounts\n"
//                + "where username=?";
//
//        try {
//
//            PreparedStatement st = connection.prepareStatement(sql);
//            st.setString(1, user);
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                return new Account(
//                        rs.getString("username"),
//                        rs.getString("password"),
//                        rs.getInt("isCustomer"),
//                        rs.getInt("isAdmin")
//                );
//            }
//        }
//        catch (SQLException e) {
//            System.out.println(e);
//        }
//        return null;
//    }

    public static void main(String[] args) {
        AccountDAO d = new AccountDAO();
        List<Account> list = d.getAllAccounts();
        for (Account o : list) {
            System.out.println(o);
        }
    }
}
