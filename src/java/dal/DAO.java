/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import model.Material;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Products;

/**
 *
 * @author Acer
 */
public class DAO extends DBContext {

    public List<Products> getAllProducts() {
        List<Products> list = new ArrayList<>();
        String sql = "SELECT * FROM Products";
        System.out.println("SQL Query: " + sql);

        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Products p = new Products(
                        rs.getInt("iditem"),
                        rs.getString("item"),
                        rs.getString("material"),
                        rs.getFloat("price"),
                        rs.getInt("quantity"),
                        rs.getString("catid"),
                        rs.getString("image")
                );
                list.add(p);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Products> getProductsByCatId(String catid) {
        List<Products> list = new ArrayList<>();
        String sql = "Select * from Products\n"
                + "where catid=?";
        System.out.println("SQL Query: " + sql);

        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, catid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Products p = new Products(
                        rs.getInt("iditem"),
                        rs.getString("item"),
                        rs.getString("material"),
                        rs.getFloat("price"),
                        rs.getInt("quantity"),
                        rs.getString("catid"),
                        rs.getString("image")
                );
                list.add(p);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Material> getAllMaterial() {
        List<Material> listC = new ArrayList<>();
        String sql = "SELECT * FROM Material";
        System.out.println("SQL Query: " + sql);

        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Material m = new Material(rs.getInt("id"), rs.getString("material"));
                listC.add(m);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return listC;
    }

    public List<Products> getProductsByMaterial(String m) {
        List<Products> list = new ArrayList<>();
        String sql = "Select * from Products\n"
                + "where material=?";
        System.out.println("SQL Query: " + sql);

        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, m);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Products p = new Products(
                        rs.getInt("iditem"),
                        rs.getString("item"),
                        rs.getString("material"),
                        rs.getFloat("price"),
                        rs.getInt("quantity"),
                        rs.getString("catid"),
                        rs.getString("image")
                );
                list.add(p);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

//    public List<Categories> getProductsById(String iditem) {
//        List<Categories> list = new ArrayList<>();
//        String sql = "Select * from Categories\n"
//                + "where iditem=?";
//        System.out.println("SQL Query: " + sql);
//
//        try {
//
//            PreparedStatement st = connection.prepareStatement(sql);
//            st.setString(1, iditem);
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                Categories c = new Categories(rs.getString("catid"), rs.getInt("iditem"), rs.getString("item"));
//                list.add(c);
//            }
//        }
//        catch (SQLException e) {
//            System.out.println(e);
//        }
//        return list;
//    }
    public Products getProductsByIditem(String iditem) {
        String sql = "Select * from Products\n"
                + "where iditem=?";

        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, iditem);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Products(
                        rs.getInt("iditem"),
                        rs.getString("item"),
                        rs.getString("material"),
                        rs.getFloat("price"),
                        rs.getInt("quantity"),
                        rs.getString("catid"),
                        rs.getString("image")
                );
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

//    public Products getLast() {
//        String sql = "Select * from Products\n"
//                + "order by Products.iditem desc";
//        try {
//            PreparedStatement st = connection.prepareStatement(sql);
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                Products p = new Products(
//                        rs.getInt("iditem"),
//                        rs.getString("item"),
//                        rs.getString("material"),
//                        rs.getFloat("price"),
//                        rs.getInt("quantity"),
//                        rs.getString("catid"),
//                        rs.getString("image")
//                );
//                return p;
//            }
//        }
//        catch (SQLException e) {
//            System.out.println(e);
//        }
//        return null;
//    }
    public Account login(String username, String password) {
        String sql = "Select * from Accounts\n"
                + "where username =?\n"
                + "and password =?";

        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            st.setString(2, password);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(rs.getString("username"), rs.getString("password"), rs.getInt("isCustomer"), rs.getInt("isAdmin"));
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public Account checkAccountExist(String username) {
        String sql = "Select * from Accounts\n"
                + "where username =?\n";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, username);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return new Account(rs.getString("username"), rs.getString("password"), rs.getInt("isCustomer"), rs.getInt("isAdmin"));
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void signUp(String user, String pass) {
        String sql = "INSERT INTO Accounts VALUES (?, ?, 1, 0)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, user);
            st.setString(2, pass);
            st.executeUpdate();
        }
        catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<Products> getProductsByName(String txtSearch) {
        List<Products> list = new ArrayList<>();
        String sql = "Select * from Products\n"
                + "where item like ?";
        System.out.println("SQL Query: " + sql);
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + txtSearch + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Products p = new Products(
                        rs.getInt("iditem"),
                        rs.getString("item"),
                        rs.getString("material"),
                        rs.getFloat("price"),
                        rs.getInt("quantity"),
                        rs.getString("catid"),
                        rs.getString("image"));
                list.add(p);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Products> getPriceAsc() {
        List<Products> list = new ArrayList<>();
        String sql = "Select * from Products\n"
                + "order by price asc";
        System.out.println("SQL Query: " + sql);
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Products p = new Products(
                        rs.getInt("iditem"),
                        rs.getString("item"),
                        rs.getString("material"),
                        rs.getFloat("price"),
                        rs.getInt("quantity"),
                        rs.getString("catid"),
                        rs.getString("image"));
                list.add(p);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Products> getQuantityAsc() {
        List<Products> list = new ArrayList<>();
        String sql = "Select * from Products\n"
                + "order by quantity asc";
        System.out.println("SQL Query: " + sql);
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Products p = new Products(
                        rs.getInt("iditem"),
                        rs.getString("item"),
                        rs.getString("material"),
                        rs.getFloat("price"),
                        rs.getInt("quantity"),
                        rs.getString("catid"),
                        rs.getString("image"));
                list.add(p);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Products> getProductAlphabet() {
        List<Products> list = new ArrayList<>();
        String sql = "Select * from Products\n"
                + "order by item asc";
        System.out.println("SQL Query: " + sql);
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Products p = new Products(
                        rs.getInt("iditem"),
                        rs.getString("item"),
                        rs.getString("material"),
                        rs.getFloat("price"),
                        rs.getInt("quantity"),
                        rs.getString("catid"),
                        rs.getString("image"));
                list.add(p);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Products> getProductsByCatIdAndMaterial(String s) {
        List<Products> list = new ArrayList<>();
         String sql = s;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Products p = new Products(
                        rs.getInt("iditem"),
                        rs.getString("item"),
                        rs.getString("material"),
                        rs.getFloat("price"),
                        rs.getInt("quantity"),
                        rs.getString("catid"),
                        rs.getString("image")
                );
                list.add(p);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public static void main(String[] args) {
        DAO d = new DAO();
        List<Material> list = d.getAllMaterial();
        for (Material o : list) {
            System.out.println(o);
        }
    }
}
