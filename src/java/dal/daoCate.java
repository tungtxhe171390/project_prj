/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Categories;

/**
 *
 * @author Acer
 */
public class daoCate extends DBContext{
    public List<Categories> getAllCategories() {
        List<Categories> list = new ArrayList<>();
        String sql =  "Select * from Categories";
        
        System.out.println("SQL Query: " + sql);

        try {

            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Categories c = new Categories(rs.getString("catid"), rs.getString("code"), rs.getString("cname"));
                list.add(c);
            }
        }
        catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }
    
    public static void main(String[] args) {
        daoCate d= new daoCate();
          List<Categories> list = d.getAllCategories();
                  for(Categories o: list){
                      System.out.println(o);
                  }
    }
}
