<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>LACOSTE</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    </head>

    <body>
        <jsp:include page="Menu.jsp"></jsp:include>
        <% 
String successMessage = (String) request.getAttribute("successMessage");
if (successMessage != null) {
        %>
        <script type="text/javascript">
            window.onload = function () {
                setTimeout(function () {
                    alert('<%= successMessage %>');
                }, 500); // trì hoãn 0.5 giây
            };
        </script>
        <%
            }
        %>
        <!--        <div class="hero__item set-bg" data-setbg="img/ava.avif">
                    <div class="hero__text">
                        <span>Lacoste gifts: the wishlist</span>
                        <h2>COLLECTIONS <br />2023</h2>
                        <p>Free Pickup and Delivery Available</p>
                        <a href="Login.jsp" class="primary-btn">SHOP NOW</a>
                    </div>
                </div>-->
        <form action="extrasearch" method="POST">
            <div style="float: left; width: 50%;">
                <h3>Categories:</h3>
                <c:forEach items="${listCC}" var="c">
                    <input type="checkbox" name="catid" value="${c.catid}" class="custom-checkbox">${c.cname}<br/>
                </c:forEach>
            </div>

            <div style="float: left; width: 50%;">
                <h3>Materials:</h3>
                <input type="checkbox" name="material" value="Cotton"> Cotton
                <input type="checkbox" name="material" value="Polyester"> Polyester</br>
                <input type="checkbox" name="material" value="Rayon"> Rayon
                <input type="checkbox" name="material" value="Virgin wool"> Virgin wool</br>
                <input type="checkbox" name="material" value="Nilon"> Nilon
            </div>

            <div style="clear: both;"></div>

            <button type="submit" class="site-btn">SEARCH</button>
        </form>

    </div>
</div>
</div>
</section>
<!-- Hero Section End -->

<!-- Featured Section Begin -->
<section class="featured spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h2>Featured Product</h2>
                </div>
                <div class="featured__controls">
                    <ul>
                        <li class="active" data-filter="*">All</li>
                        <li class="active" data-filter=".oranges">
                            <c:forEach items="${listM}" var="m">
                            <li><a style="color: green" href="material?material=${m.material}">${m.material}</a></li>
                            </c:forEach>       
                        </li>  
                    </ul>
                </div>
            </div>
        </div>


        <div class="row featured__filter">
            <c:forEach items="${listP}" var="p">
                <div class="col-lg-3 col-md-4 col-sm-6 mix oranges fresh-meat" style="height: 400px">
                    <div class="featured__item">
                        <div class="featured__item__pic set-bg" data-setbg="Images/${p.image}">
                            <ul class="featured__item__pic__hover">
                                <li><a href="#"><i class="fa fa-heart"></i></a></li>
                                <li><a href="detail?iditem=${p.iditem}"><i class="fa fa-info-circle"></i></a></li>
                                <li><a href="detail?iditem=${p.iditem}"><i class="fa fa-shopping-cart"></i></a></li>   
                            </ul>  
                        </div>


                        <div class="featured__item__text">
                            <h6>${p.item}</h6>
                            <h5>${p.price}</h5>


                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
        <!-- Featured Section End -->

    </div>
</section>
<div class="section-title">
    <h2>New Products 2023</h2>
</div>

<!-- Categories Section Begin -->
<section class="categories">
    <div class="container">
        <div class="row">
            <div class="categories__slider owl-carousel">
                <div class="col-lg-3">
                    <div class="categories__item set-bg" data-setbg="img/jogger.avif">
                        <h5><a href="Login.jsp">Jogger Sweatshirt</a></h5>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="categories__item set-bg" data-setbg="img/knitwear.avif">
                        <h5><a href="Login.jsp">V Neck Knitwear</a></h5>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="categories__item set-bg" data-setbg="img/coast.avif">
                        <h5><a href="Login.jsp">Fiber Zipped Coast</a></h5>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="categories__item set-bg" data-setbg="img/homewear.jpg">
                        <h5><a href="Login.jsp">Pyjama Homewear</a></h5>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="categories__item set-bg" data-setbg="img/socks.avif">
                        <h5><a href="Login.jsp">Unisex Socks</a></h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Categories Section End -->

<div class="section-title">
    <h2>Hot Collections 2023</h2>
</div>
<!-- Banner Begin -->
<div class="banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="banner__pic">
                    <img style="height: 416px; width: 600px" src="img/summer.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="banner__pic">
                    <img style="height: 416px; width: 600px" src="img/blackfriday.png" alt="">
                </div>
            </div>

        </div>
    </div>
</div>
</div>
</div>
<!-- Banner End -->

<!-- Latest Product Section Begin -->
<section class="latest-product spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-6">
                <div class="latest-product__text">
                    <h4>A - Z Product</h4>                
                    <div class="latest-product__slider owl-carousel">
                        <c:forEach items="${a}" var="a" varStatus="loop">
                            <c:if test="${loop.index % 3 == 0}">
                                <div class="latest-prdouct__slider__item">
                                </c:if>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="Images/${a.image}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>${a.item}</h6>
                                        <span>Price: ${a.price}</span>
                                        <span>Quantity: ${a.quantity}</span>
                                    </div>
                                </a>
                                <c:if test="${loop.index % 3 == 2 or loop.last}">
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="latest-product__text">
                    <h4>Price Ascending</h4>   
                    <div class="latest-product__slider owl-carousel">
                        <c:forEach items="${price}" var="o" varStatus="loop">
                            <c:if test="${loop.index % 3 == 0}">
                                <div class="latest-prdouct__slider__item">
                                </c:if>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="Images/${o.image}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>${o.item}</h6>
                                        <span>Price: ${o.price}</span>
                                    </div>
                                </a>
                                <c:if test="${loop.index % 3 == 2 or loop.last}">
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6">
                <div class="latest-product__text">
                    <h4>Top Quantity Products</h4>
                    <div class="latest-product__slider owl-carousel">
                        <c:forEach items="${quantity}" var="qua" varStatus="loop">
                            <c:if test="${loop.index % 3 == 0}">
                                <div class="latest-prdouct__slider__item">
                                </c:if>
                                <a href="#" class="latest-product__item">
                                    <div class="latest-product__item__pic">
                                        <img src="Images/${qua.image}" alt="">
                                    </div>
                                    <div class="latest-product__item__text">
                                        <h6>${qua.item}</h6>
                                        <span>Quantity: ${qua.quantity}</span>
                                    </div>
                                </a>
                                <c:if test="${loop.index % 3 == 2 or loop.last}">
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- Latest Product Section End -->

<!-- Blog Section Begin -->
<section class="from-blog spad">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title from-blog__title">
                    <h2>From The Blog</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog__item">
                    <div class="blog__item__pic">
                        <img src="img/blog/blog-1.jpg" alt="">
                    </div>
                    <div class="blog__item__text">
                        <ul>
                            <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                            <li><i class="fa fa-comment-o"></i> 5</li>
                        </ul>
                        <h5><a href="#">Cooking tips make cooking simple</a></h5>
                        <p>Sed quia non numquam modi tempora indunt ut labore et dolore magnam aliquam quaerat </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog__item">
                    <div class="blog__item__pic">
                        <img src="img/blog/blog-2.jpg" alt="">
                    </div>
                    <div class="blog__item__text">
                        <ul>
                            <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                            <li><i class="fa fa-comment-o"></i> 5</li>
                        </ul>
                        <h5><a href="#">6 ways to prepare breakfast for 30</a></h5>
                        <p>Sed quia non numquam modi tempora indunt ut labore et dolore magnam aliquam quaerat </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="blog__item">
                    <div class="blog__item__pic">
                        <img src="img/blog/blog-3.jpg" alt="">
                    </div>
                    <div class="blog__item__text">
                        <ul>
                            <li><i class="fa fa-calendar-o"></i> May 4,2019</li>
                            <li><i class="fa fa-comment-o"></i> 5</li>
                        </ul>
                        <h5><a href="#">Visit the clean farm in the US</a></h5>
                        <p>Sed quia non numquam modi tempora indunt ut labore et dolore magnam aliquam quaerat </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Blog Section End -->

<jsp:include page="Footer.jsp"></jsp:include>

<!-- Js Plugins -->
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/jquery-ui.min.js"></script>
<script src="js/jquery.slicknav.js"></script>
<script src="js/mixitup.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/main.js"></script>

</body>

</html>