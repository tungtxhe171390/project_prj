<%-- 
    Document   : CRUDAccount
    Created on : Nov 2, 2023, 2:32:24 PM
    Author     : Acer
--%>

<%-- 
    Document   : Cart
    Created on : Oct 27, 2023, 8:32:04 PM
    Author     : Acer
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zxx">

    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Ogani | Template</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
    </head>

    <body>

        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="home"><img src="img/logo2.png" alt=""></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul>
                            <li class="active"><a href="./HomePage.jsp">Home</a></li>
                            <li><a href="./ShopDetail.jsp">Shop</a>
                                <ul class="header__menu__dropdown">
                                    <li><a href="./shop-details.html">Shop Details</a></li>
                                    <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                                    <li><a href="./checkout.html">Check Out</a></li>
                                    <li><a href="./blog-details.html">Blog Details</a></li>
                                </ul>
                            </li>
                            <li><a href="Cart.jsp">Cart</a></li>
                            <li><a href="./blog.html">Feedback</a></li>
                            <li><a href="./Contact.jsp">Contact</a></li>
                        </ul>
                    </nav>
                </div>                  

                <div class="col-lg-3">
                    <div class="header__cart">
                        <ul>
                            <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>3</span></a></li>
                        </ul>
                        <div class="header__cart__price">item: <span>$150.00</span></div>
                    </div>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header>
    <!-- Hero Section End -->

    <!-- Breadcrumb Section Begin -->
    <div class="hero__item set-bg" data-setbg="img/ava.avif">
        <div class="hero__text">

        </div>
    </div>
    <!-- Breadcrumb Section End -->
    <div class="hero__search__form" style="margin-left: 30%">

        <form action="crudacc" method="POST">
            <div class="hero__search__categories">
                All Categories
                <span class="arrow_carrot-down"></span>
            </div>
            <input type="text" placeholder="What do you need?" name="txt" value="${txtS}">
            <button type="submit" class="site-btn">SEARCH</button>
        </form>

    </div>
    <!-- Shoping Cart Section Begin -->
    <section class="shoping-cart spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__table">
                        <table>
                            <thead>
                                <tr >
                                    <th >Account</th>
                                    <th>Password</th>
                                    <th>Customer</th>
                                    <th>Admin</th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${listP}" var="p">
                                    <tr>
                                        <td class="shoping__cart__price">
                                            ${p.username}
                                        </td>
                                        <td class="shoping__cart__price">
                                            ${p.password}
                                        </td>
                                        <td class="shoping__cart__price">
                                            ${p.isCustomer}
                                        </td>     
                                        <td class="shoping__cart__price">
                                            ${p.isAdmin}
                                        </td>

                                        <td><a href="updateacc?id=${p.username}">Acceptable Use Policy</a></td>
                                        <td><a style="color: red" href="updateacc?id=${p.username}&type=0">REMOVE</a></td>
                                    </tr>
                                </c:forEach>
                            </tbody>

                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="shoping__cart__btns">
                        <a style="background-color: green; color: white" href="home" class="primary-btn cart-btn">Back to home</a>
                        <a style="background-color: green; color: white" href="CreateAccount.jsp" class="primary-btn cart-btn cart-btn-right"><span class="icon_loading"></span>
                            Create Account</a>
                    </div>
                </div>
                
                <div class="col-lg-6">
                    <div class="shoping__continue">
                        <div class="shoping__discount">
                            <h5>Discount Codes</h5>
                            <form action="#">
                                <input type="text" placeholder="Enter your coupon code">
                                <button type="submit" class="site-btn">APPLY COUPON</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="shoping__checkout">
                        <h5>Cart Total</h5>
                        <ul>
                            <li>Subtotal <span>$454.98</span></li>
                            <li>Total <span>$454.98</span></li>
                        </ul>
                        <a href="#" class="primary-btn">PROCEED TO CHECKOUT</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Shoping Cart Section End -->

    <!-- Footer Section Begin -->
    <jsp:include page="Footer.jsp"></jsp:include>
    <!-- Footer Section End -->

    <!-- Js Plugins -->
    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.nice-select.min.js"></script>
    <script src="js/jquery-ui.min.js"></script>
    <script src="js/jquery.slicknav.js"></script>
    <script src="js/mixitup.min.js"></script>
    <script src="js/owl.carousel.min.js"></script>
    <script src="js/main.js"></script>


</body>

</html>
