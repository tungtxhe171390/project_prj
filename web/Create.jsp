<%-- 
    Document   : Create
    Created on : Oct 30, 2023, 12:03:18 AM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <form action="create" method="post">
            <h1>Create Product </h1>         

            <table>
                <tr>  
                    <td>ID</td>
                    <td><input type="number" name="id" required=""><br/></td>
                </tr>

                <tr>  
                    <td>Item</td>
                    <td><input type="text" name="item" required=""><br/></td>
                </tr>

                <tr>  
                    <td>Material</td>
                    <td> <select name="material">
                            <option value="Cotton">Cotton</option>
                            <option value="Polyester"> Polyester</option>
                            <option value="Rayon"> Rayon</option>
                            <option value="Virgin wool"> Virgin wool</option>
                            <option value="Nilon">Nilon</option>
                        </select> <br/>
                    </td>
                </tr>

                <tr>  
                    <td>Price</td>
                    <td><input type="number" name="price" required=""><br/></td>
                </tr>

                <tr>  
                    <td>Quantity</td>
                    <td><input type="number" name="quantity" required=""><br/></td>
                </tr>

                <tr>  
                    <td>CatID</td>
                    <td> <select name="catid">
                            <option value="SS">SS</option>
                            <option value="KW">KW</option>
                            <option value="Co">Co</option>
                            <option value="HW">HW</option>
                            <option value="TS">TS</option>
                            <option value="So">So</option>
                            <option value="Bs">Bs</option>
                            <option value="PL">PL</option>
                            <option value="LG">LG</option>
                            <option value="Tros">Tros</option>
                            <option value="Ds">Ds</option>
                        </select> <br/>
                    </td>
                </tr>

                <tr>
                    <td>Image:</td>
                    <td><input type="file" name="image" accept="image/*" required><br></td>
                </tr>

            </table>

            <input type="submit" value="Create">
        </form>
    </body>
</html>
