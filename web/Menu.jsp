<%-- 
    Document   : Menu
    Created on : Oct 25, 2023, 5:22:35 AM
    Author     : Acer
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>


<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Humberger Begin -->
<div class="humberger__menu__overlay"></div>
<div class="humberger__menu__wrapper">
    <div class="humberger__menu__logo">
        <a href="home"><img style="width: 20%;height: 10%" src="img/logo2.png" alt=""></a>
    </div>
    <div class="humberger__menu__cart">
        <ul>
            <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                <c:set var="size" value="${sessionScope.size}"/>
            <li><a href="Cart.jsp"><i class="fa fa-shopping-bag"></i> <span>${size}</span></a></li>

        </ul>
        <div class="header__cart__price">item: <span>$150.00</span></div>
    </div>
    <div class="humberger__menu__widget">
        <div class="header__top__right__language">
            <img src="img/language.png" alt="">
            <div>English</div>
            <span class="arrow_carrot-down"></span>
            <ul>
                <li><a href="#">VietNamese</a></li>
                <li><a href="#">English</a></li>
            </ul>
        </div>

        <div class="header__top__right__auth">
            <a href="Login.jsp"><i class="fa fa-sign-out"></i> Log out</a>
        </div>


    </div>
    <nav class="humberger__menu__nav mobile-menu">
        <ul>
            <li class="active"><a href="home">Home</a></li>
            <li><a href="./ShopDetail.jsp">Shop</a></li>
            <li><a href="Cart.jsp">Cart</a>
                <ul class="header__menu__dropdown">
                    <li><a href="./shop-details.html">Shop Details</a></li>
                    <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                    <li><a href="./checkout.html">Check Out</a></li>
                    <li><a href="./blog-details.html">Blog Details</a></li>
                </ul>
            </li>
            <li><a href="./blog.html">Feedback</a></li>
            <li><a href="./Contact.jsp">Contact</a></li>
        </ul>
    </nav>
    <div id="mobile-menu-wrap"></div>
    <div class="header__top__right__social">
        <a href="#"><i class="fa fa-facebook"></i></a>
        <a href="#"><i class="fa fa-twitter"></i></a>
        <a href="#"><i class="fa fa-linkedin"></i></a>
        <a href="#"><i class="fa fa-pinterest-p"></i></a>
    </div>

    <div class="humberger__menu__contact">
        <ul>
            <li><i class="fa fa-envelope"></i> tungtxhe171390@fpt.edu.vn</li>
                <c:if test="${acc.isAdmin==1}" >
                <a href="Info.jsp"> <li><i class="glyphicon glyphicon-user"></i>ADMIN</li></a>
                <li><a href="manager" style="margin-left: 20px">CRUD</a></li>
                </c:if>

            <c:if test="${acc.isCustomer==1}" >
                <a href="Info.jsp"><li><i class="glyphicon glyphicon-user"></i>CUSTOMER</li></a>
                    </c:if>
        </ul>
    </div>


</div>
<!-- Humberger End -->

<!-- Header Section Begin -->
<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__left">

                        <ul>
                            <li><i class="fa fa-envelope"></i> tungtxhe171390@fpt.edu.vn</li>
                                <c:if test="${acc.isAdmin==1}" >                                
                                <a href="Info.jsp"><li><i class="glyphicon glyphicon-user"></i>ADMIN  </li>   </a>    
                                <li><a href="manager"  style="margin-left: 20px">CRUD</a></li>
                                </c:if>

                            <c:if test="${acc.isCustomer==1}" >
                                <a href="Info.jsp"><li><i class="glyphicon glyphicon-user"></i>CUSTOMER</li></a>
                                    </c:if>
                        </ul>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__right">
                        <div class="header__top__right__social">
                            <a href="https://www.facebook.com/friendzonesomeoneis"><i class="fa fa-facebook"></i></a>
                            <a href="https://www.instagram.com/tunggtungg_neverfollowsgirls/"><i class="fa fa-instagram"></i></a>
                            <a href="https://twitter.com/TunggTungg_Fake"><i class="fa fa-twitter"></i></a>
                        </div>
                        <div class="header__top__right__language">
                            <img src="img/language.png" alt="">
                            <div>English</div>
                            <span class="arrow_carrot-down"></span>
                            <ul>
                                <li><a href="#">Vietnamese</a></li>
                                <li><a href="#">English</a></li>
                            </ul>
                        </div>

                        <div class="header__top__right__auth">
                            <a href="Login.jsp"><i class="fa fa-sign-out"></i> Log out</a>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="header__logo">
                    <a href="home"><img src="img/logo2.png" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6">
                <nav class="header__menu">
                    <ul>
                        <li class="active"><a href="home">Home</a></li>
                        <li><a href="./ShopDetail.jsp">Shop</a>
                            <ul class="header__menu__dropdown">
                                <li><a href="./shop-details.html">Shop Details</a></li>
                                <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                                <li><a href="./checkout.html">Check Out</a></li>
                                <li><a href="./blog-details.html">Blog Details</a></li>
                            </ul>
                        </li>
                        <li><a href="Cart.jsp">Cart</a></li>
                        <li><a href="./blog.html">Feedback</a></li>
                        <li><a href="Contact.jsp">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <div class="col-lg-3">
                <div class="header__cart">
                    <ul>
                        <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                            <c:set var="size" value="${sessionScope.size}"/>                     
                        <li><a href="Cart.jsp"><i class="fa fa-shopping-bag"></i> <span>${size}</span></a></li>
                    </ul>
                    <div class="header__cart__price">item: <span>$150.00</span></div>
                </div>
            </div>
        </div>
        <div class="humberger__open">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>
<!-- Header Section End -->

<!-- Hero Section Begin -->
<section class="hero">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="hero__categories">
                    <div class="hero__categories__all">
                        <i class="fa fa-bars"></i>
                        <span>All departments</span>
                    </div>
                    <ul>
                        <c:forEach items="${listCC}" var="c">
                            <li><a href="categorycontroller?catid=${c.catid}">${c.cname}</a></li>
                            </c:forEach>

                    </ul>
                </div>
            </div>

            <div class="col-lg-9">
                <div class="hero__search">
                    <div class="hero__search__form">

                        <form action="search" method="POST">
                            <div class="hero__search__categories">
                                All Categories
                                <span class="arrow_carrot-down"></span>
                            </div>
                            <input type="text" placeholder="What do you need?" name="txt" value="${txtS}">
                            <button type="submit" class="site-btn">SEARCH</button>
                        </form>

                    </div>
                    <div class="hero__search__phone">
                        <div class="hero__search__phone__icon">
                            <i class="fa fa-phone"></i>
                        </div>
                        <div class="hero__search__phone__text">
                            <h5>036.750.8291</h5>
                            <span>support 24/7 time</span>
                        </div>
                    </div>
                </div>
