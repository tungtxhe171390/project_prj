<%-- 
    Document   : CreateAccount
    Created on : Oct 22, 2023, 3:17:07 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="description" content="Ogani Template">
        <meta name="keywords" content="Ogani, unica, creative, html">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>LACOSTE</title>

        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;900&display=swap" rel="stylesheet">

        <!-- Css Styles -->
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
        <link rel="stylesheet" href="css/elegant-icons.css" type="text/css">
        <link rel="stylesheet" href="css/nice-select.css" type="text/css">
        <link rel="stylesheet" href="css/jquery-ui.min.css" type="text/css">
        <link rel="stylesheet" href="css/owl.carousel.min.css" type="text/css">
        <link rel="stylesheet" href="css/slicknav.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">


    </head>
    <body>
        <div class="contact-form spad">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="contact__form__title">
                            <h2>CHANGE PASSWORD ACCOUNT</h2>
                        </div>
                    </div>
                </div>
                <form action="changepass" method="POST">
                    <div class="row">
                        <div class="col-lg-12 text-center">
                            <b>Your Account</b></br>
                            <input type="hidden" name="user" value="${acc.username}">
                            <td><b><button style="font-weight: bold;">${acc.username}</button></b></td>
                        </div>
                        <div class="col-lg-12 text-center">
                            <b>Your Password</b>
                            <input type="text" placeholder="Your Password" name="a1" required>
                        </div>
                        <div class="col-lg-12 text-center">
                            <b>Input Your Password Again</b>
                            <input type="text" placeholder="Input Your Password Again" name="a2" required>                         
                        </div>
                        <p class="error-message">${mess}</p>
                        <div class="col-lg-12 text-center">
                            <input style="color: green" type="submit" value="SAVE CHANGE">
                        </div>
                    </div> 
                </form>

            </div>

            <script src="js/jquery-3.3.1.min.js"></script>
            <script src="js/bootstrap.min.js"></script>
            <script src="js/jquery.nice-select.min.js"></script>
            <script src="js/jquery-ui.min.js"></script>
            <script src="js/jquery.slicknav.js"></script>
            <script src="js/mixitup.min.js"></script>
            <script src="js/owl.carousel.min.js"></script>
            <script src="js/main.js"></script>
    </body>
</html>
