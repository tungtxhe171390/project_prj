<%-- 
    Document   : Update
    Created on : Oct 28, 2023, 1:34:50 AM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <tr>
                <th>Product ID:</th>
                <td>${detailP.iditem}</td>
            </tr>
            <form action="update" method="post">
                <input type="hidden" name="id" value="${detailP.iditem}">

                <tr>
                    <th>Item:</th>
                    <td><input type="text" name="item" value="${detailP.item}"></td>
                </tr>

                <tr>  
                    <td>Material</td>
                    <td> <select name="material">
                            <option value="Cotton">Cotton</option>
                            <option value="Polyester"> Polyester</option>
                            <option value="Rayon"> Rayon</option>
                            <option value="Virgin wool"> Virgin wool</option>
                            <option value="Nilon">Nilon</option>
                        </select> <br/>
                    </td>
                </tr>

                <tr>
                    <th>Price:</th>
                    <td><input type="number" name="price" value="${detailP.price}"></td>
                </tr>

                <tr>
                    <th>Quantity:</th>
                    <td><input type="number" name="quantity" value="${detailP.quantity}"></td>
                </tr>

                <tr>
                    <th>Image:</th>
                    <td><input type="text" name="image" value="${detailP.image}"></td>
                </tr>

                <tr>
                    <td colspan="2">
                        <input type="submit" value="Update">
                    </td>
                </tr>
            </form>
        </table>


    </body>
</html>
